from flask import Flask, render_template, request, url_for
import os
from logic import a_star

PEOPLE_FOLDER = os.path.join('static', 'people_photo')
app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = PEOPLE_FOLDER

@app.route("/")
def main():
    # start_point = request.args.get('start_point')
    # end_point = request.args.get('end_point')
    # print(get_path(start_point, end_point))
    return render_template('index.html')


@app.route("/information")
def information():
    return render_template('information.html')
    full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'salibkasih.jpg')
    return render_template("index.html", user_image = full_filename)

    
@app.route("/faq")
def faq():
    return render_template('faq.html')

if __name__ == "__main__":
    app.run(debug=True)