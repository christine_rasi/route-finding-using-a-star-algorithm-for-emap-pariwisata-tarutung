# -*- coding: utf-8 -*-
"""


@author: Christine


"""

from priority_queue import *
import colorama
from colorama import Fore, Back, Style


# HELPER
def visualize(frontier):
    colorama.init()
    
    
def build_graph_weighted(file):
    """Builds a weighted, undirected graph"""
    graph = {}
    for line in file:
        v1, v2, w = line.split(',')
        v1, v2 = v1.strip(), v2.strip()
        w = int(w.strip())
        if v1 not in graph:
            graph[v1] = []
        if v2 not in graph:
            graph[v2] = []
        graph[v1].append((v2,w))
        graph[v2].append((v1,w))
    return graph

# Helper methods for A*
def build_heuristic_dict():
    h = {}
    with open("sld_to_bucharest.txt", 'r') as file:
        for line in file:
            line = line.strip().split(",")
            node = line[0].strip()
            sld = int(line[1].strip())
            h[node] = sld
    return h

def heuristic(node, values):
    return values[node]

# A* searchsALI
def a_star(graph, start, dest, visualization=False):
    """Performs a* search on graph 'graph' with
        'start' as the beginning node and 'dest' as the goal.
        Returns shortest path from 'start' to 'dest'.
        If 'visualization' is set to True, then progress of
        algorithm is shown."""

    frontier = PriorityQueue()

    # uses helper function for heuristics
    h = build_heuristic_dict()

    # path is a list of tuples of the form ('node', 'cost')
    frontier.insert([(start, 0)], 0)
    explored = set()

    while not frontier.is_empty():

        # show progress of algorithm
        if visualization:
            visualize(frontier)

        # shortest available path
        path = frontier.remove()

        # frontier contains paths with final node unexplored
        node = path[-1][0]
        g_cost = path[-1][1]
        explored.add(node)

        # goal test:
        if node == dest:
            # return only path without cost
            return [x for x, y in path]

        for neighbor, distance in graph[node]:
            cumulative_cost = g_cost + distance
            f_cost = cumulative_cost + heuristic(neighbor, h)
            new_path = path + [(neighbor, cumulative_cost)]

            # add new_path to frontier
            if neighbor not in explored:
                frontier.insert(new_path, f_cost)

            # update costSaSim of path in frontier
            elif neighbor in frontier._queue:
                frontier.insert(new_path, f_cost)
                print(path)
    return False

with open("graph.txt", 'r') as file:
    lines = file.readlines()
print("===============================================================")
print("Lokasi Anda : \t")
start = input()  #lines[1].strip()

print("Tujuan : \t")
dest = input()   #lines[2].strip()

graph = build_graph_weighted(lines[4:])
print(a_star(graph, start, dest, True), "\n\n")